#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <limits.h>
#include <cmath>

#include "ImageUtilities.h"

using namespace std;

vector<string> &split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

int main(int argc, char *argv[]) {
	if (argc < 4) {
		cerr << "No input file, output file or target specified!" << endl;
		cerr << "Usage: ./main input-filename output-filename target-format" << endl;
		cerr << "\t\tTarget-format is grayscale or bw" << endl;

		return 1;
	}

	string inputFile(argv[1]);
	string outputFile(argv[2]);
	string target(argv[3]);

	if ((target == "grayscale") || (target == "bw")) {
		ifstream file(inputFile);
		ofstream outfile(outputFile);

		string str;

		int currentStringNumber = 0;
		vector<string> sizes;
		int width = 0;
		int height = 0;
		int maxColor = 0;

		string imageContent;

		while (getline(file, str)) {
			switch (currentStringNumber) {
			case 0:
			case 1:
				// Do nothing
				break;
			case 2:
				sizes = split(str, ' ');

				width = stoi(sizes.at(0));
				height = stoi(sizes.at(1));

				cout << "size " << width << height << endl;

				break;
			case 3:
				if (target == "bw") {
					maxColor = stoi(str);
					break;
				}
			default:
				imageContent += str + "\n";

				break;
			}

			currentStringNumber++;
		}

		if (target == "grayscale") {
			writeGrayscale(outfile, toGrayscale(imageContent, width, height), width, height);
		} else {
			writeBinary(outfile, toBW(imageContent, width, height), width, height);
		}
	} else {
		cerr << "Unknown format" << endl;
		return 1;
	}
}
