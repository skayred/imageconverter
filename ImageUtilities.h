/*
 * ImageUtilities.h
 *
 *  Created on: Dec 28, 2013
 *      Author: sk_
 */

#ifndef IMAGEUTILITIES_H_
#define IMAGEUTILITIES_H_

#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <limits.h>
#include <cmath>
#include <numeric>

using namespace std;

const int windowEdge = 3;

vector< vector<bool> > toBW(string imageContent, int width, int height) {
	vector< vector<bool> > result;
	vector< vector<int> > original;

	for (int i = 0 ; i < width ; i++) {
		vector<int> row;

		for (int j = 0 ; j < height ; j++) {
			row.push_back((int) (unsigned char) imageContent.at(i * width + j));
		}

		original.push_back(row);
	}

	int oldpixel;
	int newpixel;
	int quant_error;

	for (int i = 0; i < width - 1; i++){
		for (int j = 1; j < height - 1; j++){
			oldpixel = original[i][j];
			newpixel = (oldpixel > 128) ? 255 : 0;
			original[i][j] = newpixel;
			quant_error = oldpixel - newpixel;
			original[i][j+1] = original[i][j+1] + (7 * quant_error)/16;
			original[i+1][j-1] = original[i+1][j-1] + (3 * quant_error)/16;
			original[i+1][j] = original[i+1][j] + (5 * quant_error)/16;
			original[i+1][j+1] = original[i+1][j+1] + quant_error/16;
		}
	}

	for (int i = 0 ; i < width ; i++) {
		vector<bool> row;

		for (int j = 0 ; j < height ; j++) {
			row.push_back(original[i][j] == 0);
		}

		result.push_back(row);
	}

	return result;
}

vector< vector<int> > toGrayscale(string imageContent, int width, int height) {
	vector< vector<int> > result;
	vector< vector<int> > original;

	for (int i = 0 ; i < width ; i++) {
		vector<int> row;

		for (int j = 0 ; j < (height / (sizeof(char) * CHAR_BIT)) ; j++) {
			unsigned char symbol = (unsigned char) imageContent.at((i * ((height / (sizeof(char) * CHAR_BIT))) + j));

			for (int k = 0 ; k < sizeof(char) * CHAR_BIT ; k++) {
				row.push_back(1 - (symbol >> (sizeof(char) * CHAR_BIT - 1 - k)) & 1);
			}
		}

		original.push_back(row);
	}

	for (int i = 0 ; i < width ; i++) {
		vector<int> row;

		for (int j = 0 ; j < height ; j++) {
			row.push_back(original[i][j] * 255);
		}

		result.push_back(row);
	}

	//	Average filter
	for (int i = (windowEdge / 2) ; i < (width - (windowEdge / 2)) ; i++) {
		vector<int> row;

		for (int j = (windowEdge / 2) ; j < (height - ((windowEdge / 2))) ; j++) {
			vector<int> values;

			for (int k = 0 ; k < windowEdge ; k++) {
				for (int l = 0 ; l < windowEdge ; l++) {
					values.push_back(result[i + k - windowEdge / 2][j + l - windowEdge / 2]);
				}
			}

			result[i][j] = accumulate(values.begin(), values.end(), 0.0) / values.size();
		}

		result.push_back(row);
	}

	return result;
}

bool writeGrayscale(ofstream& f, vector<vector<int> > data, int width, int height) {
	f << "P5" << endl;
	f << "# Created by sk_ converter" << endl;
	f << width << " " << height << endl;
	f << "255" << endl;

	vector<char> linebits;
	for(size_t y = 0; y < data.size(); ++y) {
		for(size_t x = 0; x < data[y].size(); ++x) {
			linebits.push_back(data[y][x]);
		}
	}

	cout << linebits.size() << endl;
	f.write(&linebits[0], linebits.size());

	return true;
}

bool writeBinary(ofstream& f, vector<vector<bool> > data, int width, int height) {
	f << "P4" << endl;
	f << "# Created by sk_ converter" << endl;
	f << width << " " << height << endl;

	vector<char> linebits((width + (CHAR_BIT - 1)) / CHAR_BIT);
	for(size_t y = 0; y < data.size(); ++y) {
		fill(linebits.begin(), linebits.end(), 0);
		for(size_t x = 0; x < width; ++x) {
			const int bytePosition = x / CHAR_BIT;
			const int bitPosition = (CHAR_BIT - 1) - (x % CHAR_BIT);

			linebits[bytePosition] |= (data[y][x] << bitPosition);
		}

		f.write(&linebits[0], linebits.size());
	}

	return true;
}


#endif /* IMAGEUTILITIES_H_ */
